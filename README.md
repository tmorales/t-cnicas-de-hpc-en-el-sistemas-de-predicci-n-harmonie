
------------------------------------------------------
Técnicas de HPC en el Sistema de Predicción HARMONIE
-------------------------------------------------------

El objetivo de este curso es describir el Sistema de Predicción HAMONIE, desde punto de vista numérico y 
de como se implementa el código para poder ser ejecutados en supercomputadores.

El curso se compone de tres pilares fundamentales:

  * Descripción del esquema numérico y métodos numéricos empleados para discretizar las ecuaciones que el modelo mesoscalar HARMONIE tiene. 

  * Descripción de técnicas para programación en paralelo, ejercicios prácticos y HPC en el modelo HARMONIE.

  * Nuevas avances en técnicas de Asimilación de Datos en el Sistema de Predicción HARMONIE.


El curso fue impartido por tres profesores y el destinatario fue el Personal del AEMET.

Dentro de todo el curso, como profesor fuí el encargado de impartir la parte de Computación de Altas Prestaciones, tando desde
un punto académico, paradígmas de programación más empleados en la paralelización de códigos atmosféricos, como su aplicación con sesiones 
prácticas y la propia implementación del código HARMONIE.

En este proyecto podrás encontrar:

 * Presentaciones.

 * Sesión práctica:

   * Introducción a Fortran
   
   * Programación en memoria compartida usando OpenMP.

   * Programación en memoria distribuida usando MPI.

   * Programación híbrida: MPI+OpenMP. 
 

