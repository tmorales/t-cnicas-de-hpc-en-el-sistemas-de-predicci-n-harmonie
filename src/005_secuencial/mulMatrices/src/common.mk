
include ../darwin.mk

SOURCES = main.f90 sub.f90
OBJECTS = main.o sub.o

.PHONY: clean 

all: exec main.x


exec: main.x
	time ./main.x

main.x: $(SOURCES)
	$(FC) $(FFLAGS) -o main.x $(SOURCES)
	
%.o: %.f90
	$(FC) $(FFLAGS) -c $<
	
clean:
	rm -f *.o *.x