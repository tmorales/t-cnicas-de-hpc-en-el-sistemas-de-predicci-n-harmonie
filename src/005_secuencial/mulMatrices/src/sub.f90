subroutine sub(m, n, l, MA, MB, MC)

implicit none 
integer, intent(in)                       ::   m
integer, intent(in)                       ::   n
integer, intent(in)                       ::   l
real(kind=8), dimension(m,n), intent(in)  ::  MA
real(kind=8), dimension(n,l), intent(in)  ::  MB
real(kind=8), dimension(m,l), intent(out) ::  MC
! Local variables
! ---------------
integer          ::   i,j,k
real(kind=4)     ::   t1, t2
!-------------------------------------------------


call cpu_time(t1)

do j= 1,l
  do i= 1,m
  MC(i,j)= 0
    do k= 1,n
      MC(i,j)= MC(i,j) + MA(i,k) * MB(k,j)
    enddo
  enddo
enddo

call cpu_time(t2)

write(6,*) "***CPU time : ", t2-t1, " seconds"

do i= 1,10
  write(6,*) MC(i,1)
enddo


end subroutine sub
