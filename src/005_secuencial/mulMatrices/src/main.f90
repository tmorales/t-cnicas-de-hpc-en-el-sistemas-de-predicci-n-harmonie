program main

implicit none

real(kind=8), dimension(:,:), allocatable   ::  MA
real(kind=8), dimension(:,:), allocatable   ::  MB
real(kind=8), dimension(:,:), allocatable   ::  MC
integer                                     ::  m, n, l
integer                                     ::  i,j
integer                                     ::  col
integer, parameter                          ::  sizeM=2
!---------------------------------------------------------

m=2000
n=2000
l=2000

allocate(MA(m,n), MB(n,l), MC(m,l))

  do j=1,n
    do i=1,m
      MA(i,j)= 1.d0
    enddo
  enddo

  do j=1, l
    do i=1, n
      MB(i,j)= 2.d0
    enddo
  enddo

  call sub(m, n, l, MA, MB, MC)


end program main
