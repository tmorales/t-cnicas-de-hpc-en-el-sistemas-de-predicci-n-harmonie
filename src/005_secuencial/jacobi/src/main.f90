program main

implicit none
! Local variables
! ---------------
integer         ::     n
integer         ::     iter
integer         ::     maxiter
real(kind=8)    ::     tol
real(kind=8)    ::     dumax
!--------------------------------------------

n=120
tol=0.1 / (n+1)**2
tol=1.d-6
maxiter=100000

print "('Solving Laplace equation on', i4,' by', i4,' grib by Jacobi iteration')", n,n
print "('Note: This is a lousy numerical method but illustrate secuencial speed')"
print *,''

call jacabiSub(n, tol, maxiter, iter, dumax)



end program main
