program jacobiSub(n, tol, maxiter, iter, dumax)

implicit none
! Subroutine variables
! ---------------------
integer, intent(in)   ::  n
integer, intent(in)   ::  tol
integer, intent(in)   ::  maxiter
real(kind=8)          ::  iter
real(kind=8)          ::  dumax
! Local variables
! ---------------
integer                                   ::  i, j
real(kind=8)                              ::  h
real(kind=8), dimension(:)  , allocatable ::  x, y
real(kind=8), dimension(:,:), allocatable ::  u, uold, f
!----------------------------------------------------------

        ! 1.-) Grid
        !      ----
!grid spacing
h = 1.d0 / (n+1.d0)

do i= 0,n+1
  ! grid point in x:
  x(i) = i*h
  ! boundary conditions
  ! bottom boundary y=0
  u(i,0) = 0.d0
  ! top boundary y=1
  if ( (x(i) <= 0.3d0) .or. (x(i) >= 0.7d0) ) then
    u(i,n+1) = 0.d0
  else
    u(i,n+1) = 1.d0
  endif
enddo

do j= 0,n+1
  ! grib point in y
  y(i) = j*h
  ! boundary conditions
  ! left boundary y=0
  u(0,j) = 0.d0
  if ( (y(i) <= 0.3d0) .or. (y(i) >= 0.7d0) ) then
    u(n+1,j) = 0.d0
  else
    u(n+1,j) = 1.d0
  endif
enddo

do j= 1,n
  do i= 1,n
    ! source term
    f(i,j)= 0.d0
    ! initial guess
    u(i,j) = 0.d0
  enddo
enddo

        ! 2.-) Jacobi iteration
        !      ----------------
do i=1, maxiter
  uold=u
  dumax=0.d0
  do j= 1,n
    do i= 1,n
      u(i,j)= 0.25d0*(uold(i-1,j) + uold(i+1,j) + &
            &         uold(i,j-1) + uold(i,j+1) + &
            &         h**2*f(,j)

       dmax= max(dumax, abs(u(i,j) - uold(i,j))
    enddo
  enddo
  ! check the convergen:
  if (dumax .lt. tol) exit
enddo

        ! 3.-) Write output data
do j= 1,n+1
  do i= 1,n+1
    write(6,222) x(i), y(i), u(i,j), uold(i,j)
  enddo
enddo
222 format(4e20.10)

end program jacobiSub
