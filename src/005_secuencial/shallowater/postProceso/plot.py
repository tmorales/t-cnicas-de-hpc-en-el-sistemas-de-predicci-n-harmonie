
import numpy as np

try:
    fname = '../src/grid.txt'
    x, y = np.loadtxt(fname, unpack=True)
except:
    err_msg = "Could not load data from file %s." % fname \
                + " Did you forget to run the program?"
    raise Exception(err_msg)

print y

n = int(np.sqrt(len(x)))
assert n*n == len(x), "Expected len(x) to be a perfect square, len(x) = %s" % len(x)

Y = x.reshape(n,n)

print Y