subroutine geometria(psi)

use mod_variables_01, only: n, m, dx, dy, a

implicit none

real(kind=8)                              ::   pi
real(kind=8)                              ::   el
real(kind=8)                              ::   tpi
real(kind=8)                              ::   di
real(kind=8)                              ::   dj
real(kind=8)                              ::   pcf
integer                                   ::   mp1
integer                                   ::   np1
integer                                   ::   i, j
real(kind=8), dimension(:,:), allocatable ::   p
real(kind=8), dimension(:,:), allocatable ::   psi
real(kind=8), dimension(:,:), allocatable ::   U, V
!real(kind=8), dimension(:,:), allocatable, intent(out) ::   uold, vold, pold ---> intent ????
integer                                   ::   mnmin
!-----------------------------------------------------------------------------

            ! 1.-) Geometria esf�rica -----> ?????

mp1=m+1                    ! rejilla
np1=n+1                    ! rejilla

pi   =  4.d0*atan(1.d0)      ! valor de pi
tpi  =  pi+pi                ! 2*pi
el   =  n*dx                 ! tiene que equialer a la distancia de 2*pi
di   =  tpi/m                ! distancia entre puntos de la esfera (longitud)
dj   =  tpi/n                ! distancia entre puntos de la esfera (latitud)
pcf  =  pi*pi*a*a/(el*el)


            ! 2.-) Definici�n de cada rejilla (stargeteadaaaa)
            !      Arakawa C staggeedgrid
write(6,*) "***N de puntos de la rejilla de Arakawa C = ", m
write(6,*) "***N de puntos de la rejilla para construir la de Arakawa C = ", mp1
write(6,*) "***Valor de las distancia entre puntos", di


!###############################################################################################################
!###############################################################################################################
            ! paso 1: Rejilla global donde se definir�
            !         la rejilla de Arakawa C

Allocate(psi(1:mp1,1:np1), p(1:mp1,1:np1), U(1:m,1:n), V(1:m,1:n))

do j= 1, np1     !np1
  do i= 1, mp1    !mp1
    ! coordenadas esf�ricas
    ! radio tierra * sin(longitud) * sin(latitud)
    ! Representa: ��� Distancia entre puntos en metros ???
    psi(i,j)= a*sin( (float(i)-.5) * di) * sin((float(j)-.5)*dj)
    p(i,j) = pcf*(cos(2.*float(i-1)*di) + cos(2. *float(j-1)*dj)) + 50000.
  enddo
enddo

            ! Construcci�n de la rejilla de Arakawa
            ! Componentes del momento U y V
            ! -------------------------------------

            ! Desde punto +1 hasta el �ltimo de la rejilla
            ! de las componentes de U y V

do j= 1, n
  do i= 1,m
    U(i+1,j) = -(psi(i+1,j+1) - psi(i+1,j)) / dy
    V(i,j+1) = (psi(i+1,j+1) - psi(i,j+1)) / dx
  enddo
enddo

            ! Periocidad:
            ! Punto 1 de n con �ltimo de n
            ! Punto 1 de m con �ltimo de m

! periodic continuation
do j= 1,n
  U(1,j) = u(m+1,j)
  V(m+1,+1) = v(1,j+1)
enddo

do i= 1,m
  U(i+1,n+1) = u(i+1,1)
  V(i,1) = v(i,n+1)
enddo

U(1,n+1) = u(m+1,1)
V(m+1,1) = v(1,n+1)



open(unit=20, file="grid.txt", status="unknown")
  do j= 1, np1
    do i= 1, mp1
      write(20,*) "n es latitud = ", j, "m es longitud =", i, psi(i,j)
    enddo
  enddo
!222 format(3e20.10)
close(20)

do j=1,n
  do i= 1,m
    write(6,*) "u = ", U(i+1,j), "v =", V(i,j+1)
  enddo
enddo
!###############################################################################################################
!###############################################################################################################
                        ! PASO 2:
! Compute U, V, H, Z


end subroutine geometria
