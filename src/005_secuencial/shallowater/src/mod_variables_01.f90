module mod_variables_01
implicit none

integer      , parameter     ::     n=2           ! Punto de grid
integer      , parameter     ::     m=2            ! idem
real(kind=8) , parameter     ::     dx=1            ! distancia entre puntos
real(kind=8) , parameter     ::     dy=1            ! idem
!----------------------------------------------------
integer      , parameter     ::     a=1.E6
real(kind=8) , parameter     ::     alpha=0.001
!----------------------------------------------------


end module mod_variables_01
