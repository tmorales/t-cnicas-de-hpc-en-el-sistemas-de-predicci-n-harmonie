
include ../darwin.mk
 


#SOURCES = $(wildcard *.f90)  
#OBJECTS = $(subst .f90,.o,$(SOURCES))
OBJECTS  = shallowater.o geometria.o mod_variables_01.o
MODULES  = mod_variables_01.mod 

.PHONY: test 

shallowater: shallowater.x
	./shallowater.x
	
shallowater.x: $(MODULES) $(OBJECTS)
	$(FC) $(FFLAGS) -o shallowater.x $(OBJECTS)
	
	
shallowater.o: shallowater.f90
	$(FC) $(FFLAGS) -c shallowater.f90
	
	
geometria.o: geometria.f90
	$(FC) $(FFLAGS) -c geometria.f90
	
	
mod_variables_01.o: mod_variables_01.f90
	$(FC) $(FFLAGS) -c mod_variables_01.f90	


mod_variables_01.mod: mod_variables_01.f90
	$(FC) $(FFLAGS) -c mod_variables_01.f90
	

plot: grid.txt
	$(PYTHON) plot.py



clean:
	rm *.o *.x

test:
	@echo "Sources are: " $(SOURCES)
	@echo "Objects are: " $(OBJECTS)
	@echo "Modules are: " $(MODULES)