program main

use grid_variables
use earth_variables

implicit none

real                               ::    dx, dy
real, dimension(:,:), allocatable  ::    u, uold, unew
real, dimension(:,:), allocatable  ::    v, vold, vnew
real, dimension(:,:), allocatable  ::    p, pold, pnew
real, dimension(:,:), allocatable  ::    psi
integer                            ::    m, n
integer                            ::    mpi, npi

! Put in a module


!------------------------------------------------------------

! initial values of the stream function and P
do j= 1,npi
  do i= 1,mpi
    psi(i,j)= a*sin( (float(i)-.5) * di) * sin((float(j)-.5)*dj)
    p(i,j)= pcf*(cos(2.*float(i-1)*di) + cos(2.*float(j-1)*dj))+50000
  enddo
enddo

! initialize velocites
do j= 1, n
  do i= 1, m
    u(i+1,j)= -(psi(i+1,j+1) - psi(i+1,j)) / dy
    u(i,j+1)= (psi(i+1,j+1) - psi(i,j+1)) / dx
  enddo
enddo

! periodic continuation
do j= 1,n
  u(1,j) = u(m+1,j)
  v(m+1,+1) = v(1,j+1)
enddo

do i= 1,m
  u(i+1,n+1) = u(i+1,1)
  v(i,1) = v(i,n+1)
enddo

u(1,n+1) = u(m+1,1)
v(m+1,1) = v(1,n+1)

do j=1, np1
  do i= 1, mp1
    uold(i,j) = u(i,j)
    vold(i,j) = v(i,j)
    pold(i,j) = p(i,j)
  enddo
enddo

! Print initial values
write(6,390) n, m, dx, dy, dt, alpha
write(6,*)""

mnmin=min0(m,n)
write(6,391) (p(i,i), i=1,mnmin)
write(6,*)

write(6,392) (u(i,i), i=1,mnmin)
write(6,*)""

write(6,393) (v(i,i), i=1, mnmin)
write(6,*)

390 format("***Number of points in the X direction", i8,    \
           "***Number of points in the Y direction", i8,    \
           "***Grid spacing in the X direction"    , f8.0,  \
           "***Grid spacing in the Y direction"    , f8.0,  \
           "***Time step"                          , f8.0,  \
           "***Time filter parameter"              , f8.3   )

391 format(/' Initial diagonal elements of P ' //, (8e15.6))

392 format(/' Initial diagonal elements of u ' //, (8e15.6))

393 format(/' Initial diagonal elements of v ' //, (8e15.6))



















! ___________________ here ___________________



end program main
