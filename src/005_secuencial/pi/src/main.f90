program main
use omp_lib

implicit none
! Variables
! ---------
integer        ::   n
integer        ::   i
real(kind=8)   ::   dx
real(kind=8)   ::   x
real(kind=8)   ::   pi_sum
real(kind=8)   ::   pi
!-----------------------------------
print *, "***Input n ..."
!read*,n
! No lee el eclipse
n=100


dx = 1.d0/n

pi_sum=0.d0
do i = 1,n
  x = (i-0.5d0)*dx
  pi_sum = pi_sum + 1.d0 / (1.d0 + x**2)
enddo

pi = 4.d0*dx*pi_sum
write(6,*) "***The approximation of pi is = ", pi

end program main
