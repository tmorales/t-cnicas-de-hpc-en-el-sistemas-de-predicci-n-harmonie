
include ../darwin.mk

.PHONY: clean

all: exec main.x


exec: main.x
	./main.x

main.x: main.o
	$(FC) $(FFLAGS) -o main.x main.o
	
main.o: main.f90
	$(FC) $(FFLAGS) -c main.f90
	
clean:
	rm -f *.o *.x