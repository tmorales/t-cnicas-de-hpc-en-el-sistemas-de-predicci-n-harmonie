
program binwrite
implicit none
integer, dimension(20)   ::   u1, u2, u3
integer                  ::   i

!------------------------------------------

open(unit=20, file="u.bin", form="unformatted", &
   & status="unknown", access="stream")

open(unit=21, file="u.txt")

do i = 1, 20
  u1(i) = i
enddo

do i = 1, 20
  u2(i) = 2*i
enddo

do i = 1,20
  u3(i) = 3*i
enddo

do i = 1, 20
  write(21, *) u1(i), u2(i), u3(i)
enddo

do i = 1,20
  write(20,*) u1(i), u2(i), u3(i)
enddo

end program binwrite
