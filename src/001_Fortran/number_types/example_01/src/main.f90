program main
implicit none

real :: x              ! floating point number stored in 4 bytes, also known as single precision
real (kind=8) :: y, z  ! 8 bytes (corresponding to double precision in older versions of Fortran)
integer :: m
                       !1.23456789e-10 specifies a 4-byte real number.
                       !The 8-byte equivalent is 1.23456789d-10, with a d instead of e

m = 3
print *, " "
print *, "M = ",M   ! note that M==m  (case insensitive)


print *, " "
print *, "x is real (kind=4)"
x = 1.e0 + 1.23456789e-6
print *, "x = ", x


print *, " "
print *, "y is real (kind=8)"
print *, "  but 1.e0 is real (kind=4):"
y = 1.e0 + 1.23456789e-6
print *, "y = ", y


print *, " "
print *, "z is real (kind=8)"
z = 1. + 1.23456789d-6
print *, "z = ", z

end program main





