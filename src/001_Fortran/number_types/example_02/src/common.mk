
include ../darwin.mk

.PHONY: clean


main: main.x
	./main.x

main.x: main.o
	$(FC) $(FFLAGS) -o main.x main.o
	
main.o: main.f90
	$(FC) $(FFLAGS) -c main.f90
	
clean:
	rm *.o *.x