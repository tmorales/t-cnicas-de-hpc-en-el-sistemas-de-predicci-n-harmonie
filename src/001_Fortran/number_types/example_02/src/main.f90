program main
implicit none

real (kind=8) :: pi_8, x_8, y_8
real          :: pi_4, x_4, y_4
!---------------------------------------------------

! compute pi as arc-cosine of -1:
pi_8 = acos(-1.d0)  ! need -1.d0 for full precision!

x_8 = cos(pi_8)
y_8 = sqrt(exp(log(pi_8)))**2

print *, "pi = ", pi_8
print *, "x = ", x_8
print *, "y = ", y_8
print *, ""
print *, ""

pi_4 = acos(-1.e0)  ! need -1.e0 for simple precision!

x_4 = cos(pi_4)
y_4 = sqrt(exp(log(pi_4)))**2

print *, "pi = ", pi_4
print *, "x = ", x_4
print *, "y = ", y_4
print *, ""
print *, ""



end program main
