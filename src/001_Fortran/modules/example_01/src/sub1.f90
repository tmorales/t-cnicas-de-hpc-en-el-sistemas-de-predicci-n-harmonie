subroutine sub1(n, numeroIteraciones)

use variables, only: pi
implicit none

integer  ::  i
integer  ::  n
integer  ::  numeroIteraciones
real     ::  valor
!-------------------------------------

do i=1,numeroIteraciones
  valor = i*pi
  write(6,*) "***El valor es = ", valor
enddo



end subroutine sub1
