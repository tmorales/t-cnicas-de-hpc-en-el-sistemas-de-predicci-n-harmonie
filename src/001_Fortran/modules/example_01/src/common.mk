
#include ../darwin.mk

FC      =  /usr/local/bin/gfortran
FFLAGS  =  -g -O3

OBJECT=main.o sub1.o variables.o

MODULO=variables.mod


.PHONY: clean

all: main.x main

main: main.x
	./main.x

main.x: $(MODULO) $(OBJECT)
	$(FC) $(FFLAGS) -o main.x $(OBJECT)
	
main.o: main.f90
	$(FC) $(FFLAGS) -c main.f90
	
sub1.o: sub1.f90
	$(FC) $(FFLAGS) -c sub1.f90
	
variables.o: variables.f90
	$(FC) $(FFLAGS) -c variables.f90
	
variables.mod: variables.f90
	$(FC) $(FFLAGS) -c variables.f90
	
 
	
clean:
	rm -f *.o *.x *.mod
