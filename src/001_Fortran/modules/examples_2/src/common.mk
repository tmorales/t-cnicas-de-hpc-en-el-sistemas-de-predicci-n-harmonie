
include ../darwin.mk

.PHONY: clean

all: main main.x


main: main.x
	./main.x

main.x: module_1.mod main.o module_1.o
	$(FC) $(FFLAGS) -o main.x main.o module_1.o

main.o: main.f90
	$(FC) $(FFLAGS) -c main.f90

module_1.o: module_1.f90
	$(FC) $(FFLAGS) -c module_1.f90

module_1.mod: module_1.f90
	$(FC) $(FFLAGS) -c module_1.f90
	
clean:
	rm *.mod *.o *.x