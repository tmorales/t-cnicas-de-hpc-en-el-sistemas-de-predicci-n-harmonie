program main

use module_1

implicit none

real(kind=8)                   ::   pi
real(kind=8)                   ::   el
real(kind=8)                   ::   tpi
real(kind=8)                   ::   di
real(kind=8)                   ::   dj
real(kind=8)                   ::   pcf
integer                        ::   mp1
integer                        ::   np1
integer                        ::   i, j
real(kind=8), dimension(65,65) ::   p
real(kind=8), dimension(65,65) ::   psi

!----------------------------------------------

write(6,*) "***El valor de n es = ", n
write(6,*)
write(6,*) "***El valor de m es = ", m
write(6,*)
write(6,*) "***El valor de dx es = ", dx
write(6,*) ""
write(6,*) "***El valor de dy es = ", dy
write(6,*) ""
write(6,*) "***El valor de a es = ", a
write(6,*) ""
write(6,*) "***El valor de alpha es = ", alpha
write(6,*) ""
write(6,*) "================================"

el   =  n*dx               ! Distancia ?
pi   =  4.d0*atan(1.d0)    ! valor de pi
tpi  =  pi+pi
di   =  tpi/m
dj   =  tpi/n
pcf  =  pi*pi*a*a/(el*el)


write(6,*) "***El valor de Pi es = ", pi
write(6,*) "***El valor de el es = ", el
write(6,*) "***El valor de tpi es = ", tpi
write(6,*) "***El valor de di es = ", di
write(6,*) "***El valor de dj es = ", dj
write(6,*) "***El valor de pcf es = ", pcf
write(6,*) "================================"

mp1=m+1
np1=n+1

! initial values of the stream function and P
do j= 1, np1
  do i= 1, mp1
    psi(i,j) = a*sin((i-0.5d0)*di)*sin((j-0.5d0)*dj)
    p(i,j) = pcf*(cos(2.d0*(i-1.d0)*di)  + &
        &         cos(2.d0*(j-1.d0)*dj)) + 50000
  enddo
enddo

do i=1,10
  write(6,*) i, psi(i,1)
enddo




end program main
