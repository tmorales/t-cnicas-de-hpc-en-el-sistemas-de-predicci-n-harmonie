program main
implicit none

integer                                   ::  m,n
real(kind=8), dimension(:),   allocatable  ::  Aint  ! entra en la subroutine
real(kind=8), dimension(:),   allocatable  ::  Aout  ! se calcula en la subroutine y
                                                     ! se imprime en el main
real(kind=8), dimension(:,:), allocatable  ::  Bint
real(kind=8), dimension(:,:), allocatable  ::  Bout

real(kind=8), dimension(:,:), allocatable  ::  Csub
!-------------------------------------------------------
! Local variables
integer     :: i
integer     :: j
!-------------------------------------------------------

m=5
n=5

Allocate(Aint(m), Aout(m), Bint(m,n), Bout(m,n), Csub(m,n))

    call sub_1(m,n, Aint, Bint, Aout, Bout, Csub)

    ! Print matrix 1D
    Write(6,*) "***Imprimiendo valores de la matrix 1D"
    do i=1, m
      write(6,*) Aout(i)
    enddo

    ! Print matrix 2D
    write(6,*)
    write(6,*) "***Imprimiendo valores de la matrix 2D"
    do j= 1, n
      do i= 1,m
        write(6,*) "j = ", j, " i = ", j , Bout(i,j)
      enddo
    enddo

    ! Prin matrix Csub
    write(6,*)
    write(6,*) "***Imprimiendo valores de la matrix definida en la sub"
    do j= 1, n
      do i= 1,m
        write(6,*) "j = ", j, " i = ", j , Csub(i,j)
      enddo
    enddo



!Deallocate(Aint(m), Aout(m), Bint(m,n), Bout(m,n))

end program main
