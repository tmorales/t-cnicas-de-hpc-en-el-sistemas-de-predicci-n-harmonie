
include ../darwin.mk

.PHONY: clean


main: main.x
	./main.x

main.x: main.o sub_1.o
	$(FC) $(FFLAGS) -o main.x main.o sub_1.o
	
main.o: main.f90
	$(FC) $(FFLAGS) -c main.f90
	
sub_1.o: sub_1.f90
	$(FC) $(FFLAGS) -c sub_1.f90	
	
clean:
	rm *.o *.x