subroutine sub_1(m,n,Aint,Bint,  &
         &       Aout, Bout, Csub)
implicit none
integer, intent(in)           ::  m
integer, intent(in)           ::  n
real(kind=8), dimension(m)    ::  Aint
real(kind=8), dimension(m,n)  ::  Bint
!-----------------------------------------------
! Local variables
integer    ::    i
integer    ::    j
real(kind=8), dimension(m),   intent(out)    ::  Aout
real(kind=8), dimension(m,n), intent(out)    ::  Bout
real(kind=8), dimension(m,n), intent(out)    ::  Csub
!-------------------------------------------------------

            ! Matrix 1D
            ! ---------
do i= 1, m
  Aint(i) = 1.d0
enddo

do i=1,m
  Aout(i) = 2.d0*Aint(i)
enddo

            ! Matrix 2D
            ! ---------
do j=1, n
  do i= 1, m
    Bint(i,j) = 1.d0
  enddo
enddo

do j= 1,n
  do i= 1, m
    Bout(i,j) = 3.d0 * Bint(i,j)
  enddo
enddo

            ! Matrix Csub
            ! -----------
do j= 1, m
  do i= 1,n
    Csub(i,j) = 5.d0
  enddo
enddo


end subroutine sub_1
