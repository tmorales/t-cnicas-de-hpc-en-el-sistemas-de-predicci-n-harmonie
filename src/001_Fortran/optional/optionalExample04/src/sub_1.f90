subroutine sub_1(n, matrixA, matrixB)

implicit none 

integer, intent(in)                 ::  n
integer, dimension(n), intent(in)   ::  matrixA
integer, dimension(n), intent(in)   ::  matrixB
!---------------------
! Local variables
! ---------------
integer              ::  i
!-------------------------------------------------

do i=1,n
  write(6,*) matrixA(i), matrixB(i)
enddo


end subroutine sub_1
