subroutine sub_2(n, matrixA, matrixOptional)

implicit none 
integer, intent(in)                 ::  n
integer, dimension(n), intent(in)   ::  matrixA
integer, dimension(n), optional     ::  matrixOptional
!------------------------------------
! Local variables
! ---------------
integer              ::  i


!-------------------------------------------------

write(6,*) "***Estoy dentro de la subroutine sub_2"

if (present(matrixOptional)) then
  do i = 1, n
    write(6,*) matrixOptional(i)
  enddo
endif


end subroutine sub_2
