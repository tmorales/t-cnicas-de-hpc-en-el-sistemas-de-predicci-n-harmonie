program main

implicit none

interface
  subroutine sub_2(n, matrixA, matrixOptional)
    implicit none
    integer, intent(in)                 ::  n
    integer, dimension(n), intent(in)   ::  matrixA
    integer, dimension(n), optional     ::  matrixOptional
  end subroutine
end interface

integer, dimension(:), allocatable    ::   matrixA
integer, dimension(:), allocatable    ::   matrixB
integer                               ::   n
integer                               ::   i
!------------------------------------------------

!#include "sub_2.h"


n = 100

allocate(matrixA(n), matrixB(n))

do i = 1, n
  matrixA(i) = i
  matrixB(i) = i*2
enddo

! Sin optional
! ------------
call sub_1(n, matrixA, matrixB)
write(6,*) ""
write(6,*) "**********************************"

! Con optional
! ------------
call sub_2(n, matrixA, matrixOptional=matrixB)
write(6,*) ""
write(6,*) "**********************************"

! Con optional
! ------------
call sub_2(n, matrixA)
write(6,*) ""
write(6,*) "**********************************"




end program main
