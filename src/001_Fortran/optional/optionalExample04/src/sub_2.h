
interface sub_2
  subroutine sub_2(n, matrixA, matrixOptional)
    implicit none
    integer, intent(in)                 ::  n
    integer, dimension(n), intent(in)   ::  matrixA
    integer, dimension(n), optional     ::  matrixOptional
  end subroutine
end interface
