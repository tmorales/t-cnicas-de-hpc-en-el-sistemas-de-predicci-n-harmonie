subroutine sub(one, two, three, four, five)

implicit none 

integer, intent(in)              ::   one
integer, intent(in)              ::   two
integer, intent(out)             ::   three
integer, intent(in), optional    ::   four
integer, intent(out),optional    ::   five
!------------------------------------------------

three = one + two
write(6,*) "***Sum of one and two number is = ", three

write(6,*) "***Present check: ", present(four), present(five)
write(6,*) ""

if ( present(four) .and. present(five) ) then
  five = four * four
  write(6,*) "***Five value is = ", five
  write(6,*) ""
else
  write(6,*) "***Four and five values are not present ..."
  write(6,*) ""
endif

end subroutine sub
