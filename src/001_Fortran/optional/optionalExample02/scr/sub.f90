subroutine sub(logi, one, two, three, d, f)

implicit none 
integer, intent(in)            ::  one
integer, intent(in)            ::  two
integer, intent(out)           ::  three
logical, intent(in)            ::  logi
logical, intent(in), optional  ::  d
logical, intent(in), optional  ::  f
!-------------------------------------------------

write(6,*) ""
write(6,*) "***Present check: "
write(6,*) "***d variable = ", d
write(6,*) "***f variable = ", f
write(6,*) ""

if ( logi .or. present(d) .or. present(f) ) then
  write(6,*) "***Logical variable = ", logi
  write(6,*) ""

  three = one + two
  write(6,*) "***The sum of one and two values is = ", three
  write(6,*) ""
  write(6,*) "******************************************************"
else
  write(6,*) "***Logic variables = ", logi
  write(6,*) ""
  write(6,*) "******************************************************"
endif

end subroutine sub
