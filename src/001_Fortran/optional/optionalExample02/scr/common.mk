
include ../darwin.mk

SOURCES = $(wildcard *.f90)  
OBJECTS = $(subst .f90,.o,$(SOURCES))

 
.PHONY: clean clean_obj test

all: main.x main

main: main.x
	./main.x

main.x: $(OBJECTS)
	$(FC) $(FFLAGS) -o main.x $(OBJECTS)
	
%.o: %.f90
	$(FC) $(FFLAGS) -c $<
	
 
clean:
	rm -f *.o *.x  
	
clean_obj:
	rm -f *.o
	
test:
	@echo "Sources are: " $(SOURCES)
	@echo "Objects are: " $(OBJECTS)
