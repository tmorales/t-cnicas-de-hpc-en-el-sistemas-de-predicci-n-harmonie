program main


implicit none

integer          ::      one
integer          ::      two
integer          ::      three
logical          ::      a
logical          ::      b
logical          ::      c
logical          ::      d
!------------------------------------------------

one = 3
two = 6

a = .TRUE.
b = .TRUE.
c = .FALSE.
d = .TRUE.

call sub(a, one , two, three)

call sub(b, one, two, three)

call sub(c, one, two, three)

call sub(d, one, two, three)


end program main
