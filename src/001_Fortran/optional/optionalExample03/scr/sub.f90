subroutine sub(logi, one, two, three, a, b, c)

implicit none 

logical, intent(in)             ::  logi
integer, intent(in)             ::  one
integer, intent(in)             ::  two
integer, intent(out)            ::  three
logical, intent(in), optional   ::  a
logical, intent(in), optional   ::  b
logical, intent(in), optional   ::  c
!-------------------------------------------------

write(6,*) ""
write(6,*) "***Present check: "
write(6,*) "***a variable = ", present(a)
write(6,*) "***b variable = ", present(b)
write(6,*) "***c variable = ", present(c)

if ( logi .or. present(b) .or. present(c) ) then
  if ( logi ) then
    write(6,*) "***Logical variable = ", logi
    write(6,*) ""

    three = one + two
    write(6,*) "***The sum of ona and two values is = ", three
    write(6,*)
    write(6,*) "*************************************************"
  else
    write(6,*) "***Logical variables = ", logi
    write(6,*) ""
  endif
endif


end subroutine sub
