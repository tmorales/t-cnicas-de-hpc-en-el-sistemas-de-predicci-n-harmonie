program main

use modul, only : one, two, a, b

implicit none
integer         ::  three
!------------------------------------------------

a = .TRUE.
b = .FALSE.

call sub(a, one, two, three)

call sub(b, one, two, three)

end program main
