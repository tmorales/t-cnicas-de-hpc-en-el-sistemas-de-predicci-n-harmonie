
.. _modules:

Modulos
----------

**Ejercicio 1**

.. literalinclude:: ../../../src/001_Fortran/modules/example_01/src/common.mk
      :language: fortran
      :linenos:


.. literalinclude:: ../../../src/001_Fortran/modules/example_01/src/main.f90
      :language: fortran
      :linenos:


.. literalinclude:: ../../../src/001_Fortran/modules/example_01/src/variables.f90
      :language: fortran
      :linenos:


.. literalinclude:: ../../../src/001_Fortran/modules/example_01/src/sub1.f90
      :language: fortran
      :linenos:


**Ejercicio 2**


.. literalinclude:: ../../../src/001_Fortran/modules/examples_2/src/common.mk
      :language: fortran
      :linenos:


.. literalinclude:: ../../../src/001_Fortran/modules/examples_2/src/main.f90
      :language: fortran
      :linenos:


.. literalinclude:: ../../../src/001_Fortran/modules/examples_2/src/module_1.f90
      :language: fortran
      :linenos:
