
.. _optional:

Optional
--------

**Ejercicio 1**

.. literalinclude:: ../../../src/001_Fortran/optional/optionalExample01/src/common.mk 
      :language: fortran
      :linenos:

.. literalinclude:: ../../../src/001_Fortran/optional/optionalExample01/src/main.f90
      :language: fortran
      :linenos:

.. literalinclude:: ../../../src/001_Fortran/optional/optionalExample01/src/sub.f90
      :language: fortran
      :linenos:



**Ejercicio 2**

No se incluye

.. literalinclude:: ../../../src/001_Fortran/optional/optionalExample02/src/common.mk 
      :language: fortran
      :linenos:

.. literalinclude:: ../../../src/001_Fortran/optional/optionalExample02/src/main.f90
      :language: fortran
      :linenos:

.. literalinclude:: ../../../src/001_Fortran/optional/optionalExample02/src/sub.f90
      :language: fortran
      :linenos:



**Ejercicio 3**

No se incluye

.. literalinclude:: ../../../src/001_Fortran/optional/optionalExample03/src/common.mk 
      :language: fortran
      :linenos:

.. literalinclude:: ../../../src/001_Fortran/optional/optionalExample03/src/main.f90
      :language: fortran
      :linenos:

.. literalinclude:: ../../../src/001_Fortran/optional/optionalExample03/src/modul.f90
      :language: fortran
      :linenos:



**Ejercicio 4**


.. literalinclude:: ../../../src/001_Fortran/optional/optionalExample04/src/common.mk 
      :language: fortran
      :linenos:

.. literalinclude:: ../../../src/001_Fortran/optional/optionalExample04/src/main.f90
      :language: fortran
      :linenos:

.. literalinclude:: ../../../src/001_Fortran/optional/optionalExample04/src/sub_1.f90
      :language: fortran
      :linenos:

.. literalinclude:: ../../../src/001_Fortran/optional/optionalExample04/src/sub_1.f90
      :language: fortran
      :linenos:














