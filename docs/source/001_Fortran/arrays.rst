
.. _arrays:

Arrays
-----

 
.. literalinclude:: ../../../src/001_Fortran/arrays/example_01/src/common.mk 
      :language: fortran
      :linenos:

 
 
.. literalinclude:: ../../../src/001_Fortran/arrays/example_01/src/main.f90 
     :language: fortran
     :linenos:
 
 
.. literalinclude:: ../../../src/001_Fortran/arrays/example_01/src/sub_1.f90k 
     :language: fortran
     :linenos:
 


