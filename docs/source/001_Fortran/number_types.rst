
.. _number_types:

Estructura tipo type
--------------------

**Ejemplo 1**

.. literalinclude:: ../../../src/001_Fortran/number_types/example_01/src/common.mk
      :language: fortran
      :linenos:



.. literalinclude:: ../../../src/001_Fortran/number_types/example_01/src/main.f90
      :language: fortran
      :linenos:



**Ejemplo 2**

.. literalinclude:: ../../../src/001_Fortran/number_types/example_02/src/common.mk
      :language: fortran
      :linenos:


.. literalinclude:: ../../../src/001_Fortran/number_types/example_02/src/main.f90
      :language: fortran
      :linenos:



**Ejemplo 3**

.. literalinclude:: ../../../src/001_Fortran/number_types/example_03/src/common.mk
      :language: fortran
      :linenos:


.. literalinclude:: ../../../src/001_Fortran/number_types/example_03/src/main.f90
      :language: fortran
      :linenos:






